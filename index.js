var AWS = require('aws-sdk');
var DOC = require('dynamodb-doc');
var Autolinker = require( 'autolinker' );

var dynamo = new DOC.DynamoDB();

var Twitter = require('twitter-node-client').Twitter;
var twitter = new Twitter({
    "consumerKey": "",
    "consumerSecret": "",
    "accessToken": "",
    "accessTokenSecret": "",
    "callBackUrl": ""
});

var networks = ["Twitter"];

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var success = function (data) {
    console.log('Data [%s]', data);
};

var cb = function(err, data) {
    if(err) {
        console.log(err);
    } else {

    }
};

var messageOutput = "Results\n";

exports.handler = function(event, context) {
    var cb = function(err, data) {
        if(err) {
            console.log('error on RetrieveSocialFeed: ',err);
            context.done('Unable to retrieve Social Site Data', null);
        } else {
            data.Items.forEach(function(item) {
                if (item && item.SiteId) {
                    networks.forEach(function (elem) {
                        if (item[[elem] + "Status"]) {
                            loopThroughNetworks(elem, item[[elem] + "Id"], item.SiteId, item.TwitterRetweets);
                        }
                    });
                }
            });
        }
    };

    var params = {
        TableName : "SocialSiteSettings",
        FilterExpression: "SiteStatus = :siteStatus",
        ExpressionAttributeValues: {
            ":siteStatus": true,
        }
    };

    dynamo.scan(params, cb);
};

function loopThroughNetworks(network, networkId, siteId, retweets) {
    // call the correct function for the network in question returning a count of items found
    eval([network] + "Feed")(networkId, siteId, retweets);
}

function fixEntity (entity) {
  if (entity.url) {
    entity['expanded_url'] = entity.url
    entity['display_url'] = shorten(entity.url, 30)
  }
  if (entity.screenName) {
    entity.screenName = '@' + entity.screenName
    entity.indices[0] -= 1
  }
  return entity
}

function TwitterFeed(networkId, siteId, retweets) {

    twitter.getUserTimeline({ screen_name: networkId, count: 50, include_rts: retweets, exclude_replies: true, include_entities: true}, error,

        function (data) {

            var tweets = JSON.parse(data);

            console.log("\nTwitter: " + networkId + " - " + tweets.length);

            var autolinker = new Autolinker( {
                urls : {
                    schemeMatches : true,
                    wwwMatches    : true,
                    tldMatches    : true
                },
                email       : true,
                phone       : true,
                mention     : 'twitter',
                hashtag     : 'twitter',

                stripPrefix : true,
                stripTrailingSlash : true,
                newWindow   : true,

                truncate : {
                    length   : 0,
                    location : 'end'
                },

                className : ''
            } );

            for (i in tweets) {

                var DateCreated = Math.round(+new Date(Date.parse(tweets[i]['created_at'].replace(/( \+)/, ' UTC$1')))/1000);
                var TweetText = autolinker.link( tweets[i]['text'] );

                if (tweets[i].entities.media && tweets[i].entities.media[0].type == 'photo') {
                    TweetText = TweetText.concat('<span class="tweet-pic"><img src="' + tweets[i].entities.media[0].media_url_https + '"></span>');
                }

                if (typeof tweets[i]['text'] !== 'undefined') {
                    var item = {
                        ItemId: siteId + tweets[i]['id'],
                        SiteId: Number(siteId),
                        FeedType: "Twitter",
                        DateCreated: DateCreated,
                        Title: TweetText,
                        Active: true
                    };
                    dynamo.putItem({TableName:"SocialFeedsData", Item:item}, cb);
                }

            }

        }
    );

}
